Project Description 
This project implements Hotel reservation system and it is a web-based application that uses ASP.NET MVC.
This system allows customers to make online reservation.

Users' Roles:
There are two users' roles:
1.	Admin User: Who can manage any Hotel reservation.
2.	Normal User (i.e., Customer): Who can Book a Hotel.

 
Task Analysis for Normal Users: 
•	Searching for hotel 
	User can search for a desire hotel 
•	Login as a registered user 
	If a user has an account to login directly
•	Login as a new user
If a user do not have an account, he should register by filling the required information before being able to login 
•	Check-in
Determine the desirable check-in date based on the current available rooms for that given check-in date and the staying period
•	Check-out 
	Determine the desirable check-out date 
•	Select the type of room and number of guests 
There are different types of rooms that the user can choose in parallel with an option to determine the number of guests he prefers that room can accept
•	Select hotel by filtering them based on price and services 
•	User provides a credit card information to be able to book a room
•	Book the room of that Hotel 

Models: 
AccountViewModels
Calendar 
City 
Country 
CreditCard
CreditCardAttribute
CreditType
Customer
Guest
hotel
HotelRoom
HotelRoomCount
IdentityModels
ManageViewModels
reservation
RoleName
Room
State
ValidatePostal

ViewModel:
BookingViewModel
NewUserViewModel
ReservationDetailViewModel

 
The HomeRoomCount Entity for history data for maintaining count.
