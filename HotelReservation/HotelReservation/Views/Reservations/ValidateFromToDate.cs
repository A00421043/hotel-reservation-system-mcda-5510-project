﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HotelReservation.Models;

namespace HotelReservation.Views.Reservations
{
    public class ValidateFromToDate:ValidationAttribute
    {
        protected override ValidationResult IsValid(object Reservation, ValidationContext validationContext)
        {
            var reservation = (Reservation)validationContext.ObjectInstance;
            if(reservation.FromDate>reservation.ToDate)

                return ValidationResult.Success;
            else if(reservation.FromDate == reservation.ToDate)
            {
                return new ValidationResult("From date and To date can not be same.");
            }
            else
            {
                return  new ValidationResult("From date should be less then To date");
            }
        }
    }
}