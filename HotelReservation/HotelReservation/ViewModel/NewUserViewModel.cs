﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelReservation.Models;

namespace HotelReservation.ViewModel
{
    public class NewUserViewModel
    {
        //public  Type { get; set; }
        public Hotel Hotel { get; set; }

        public Reservation Reservation { get; set; }

    }
}