﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelReservation.Models;

namespace HotelReservation.ViewModel
{
    public class BookingViewModel
    {
        public Reservation Reservation { get; set; }
        public Hotel Hotel { get; set; }
        public IEnumerable<Room> Room { get; set; }
        public Customer Customer { get; set; }
        public IEnumerable<string> CreditCardType { get; set; }
        public IEnumerable<City> City { get; set; }
        public IEnumerable<State> State { get; set; }
        public IEnumerable<Country> Country { get; set; }
        
    }
}