﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelReservation.Models;

namespace HotelReservation.ViewModel
{
    public class ReservationDetailViewModel
    {
        public Hotel Hotel { get; set; }
        public Room Room { get; set; }
        public Customer Customer { get; set; }
        public Reservation Reservation { get; set; }
        public Country Country { get; set; }
        public State State { get; set; }
        public City City { get; set; }
        public CreditCard CreditCard { get; set; }
    }
}