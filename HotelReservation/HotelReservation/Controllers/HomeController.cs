﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HotelReservation.Models;
namespace HotelReservation.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index(string locationString, string searchString)
        {


            //var CountryLst = new List<string>();
            //var CountryQry = from d in db.Hotels
            //               orderby d.Country
            //               select d.Country;


            ////CountryLst.AddRange(CountryQry.Distinct());
            //ViewBag.locationString = new SelectList(CountryLst);


            //var hotels = from m in db.Hotels
            //             select m;

            //if (!string.IsNullOrEmpty(searchString))
            //{
            //    hotels = hotels.Where(s => s.Name.Contains(searchString));
            //}

            //if (!string.IsNullOrEmpty(locationString))
            //{
            //    //hotels = hotels.Where(x => x.Country == locationString);
            //}

            var hotels = db.Hotels.ToList();
            foreach (var hotel in hotels)
            {
                hotel.City = db.Cities.Single(c => c.Id == hotel.CityId);
                hotel.Country = db.Countries.Single(c => c.Id == hotel.CountryId);
                hotel.State = db.States.Single(c => c.Id == hotel.StateId);
            }
            return View(hotels);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}