﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using HotelReservation.Models;
using HotelReservation.ViewModel;
using Microsoft.AspNet.Identity;

namespace HotelReservation.Controllers
{
    public class ReservationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Reservations
        public ActionResult Index(int id)
        {

            var hotel = db.Hotels.SingleOrDefault(h => h.Id == id);
            if (hotel == null)
                RedirectToAction("Index", "Home");

            var roomType = db.Rooms.ToList();
            var countryList = db.Countries.ToList();
            var stateList = db.States.ToList();
            var cityList = db.Cities.ToList();
            var cardType = new List<String> {"Master", "Visa", "American Express"};

            BookingViewModel Booking = new BookingViewModel
            {
                Country = countryList,
                State = stateList,
                City = cityList,
                Room = roomType,
                CreditCardType = cardType,
                Hotel = hotel
            };

            return View("View", Booking);
        }

        public ActionResult Success()
        {
            return View("Success");
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Book(BookingViewModel model)
        {
            if (ModelState.IsValid)
            {
                int totalRoom = 0;

                if (Convert.ToInt32(model.Reservation.NumberOFGuest) % 2 == 0)
                {
                    totalRoom = Convert.ToInt32(model.Reservation.NumberOFGuest) / 2;
                }
                else
                {
                    totalRoom = Convert.ToInt32(model.Reservation.NumberOFGuest) / 2;
                    totalRoom = totalRoom + 1;
                }

                for (var day = model.Reservation.FromDate; day.Date < model.Reservation.ToDate; day = day.AddDays(1))
                {
                    var hotelRoom = db.HotelRooms.Where(m => m.HotelId == model.Hotel.Id)
                        .Single(m => m.RoomId == model.Reservation.HotelRoomIdStatic);
                    var hotelRoomId = hotelRoom.Id;
                    var roomId = hotelRoom.RoomId;
                    HotelRoomCount hotelRoomCount;
                    var availableInCountTable =
                        db.HotelRoomCount.Where(m => m.Date == day && m.HotelRoomId == hotelRoomId);
                    if (availableInCountTable.ToList().Count > 0)
                    {
                        hotelRoomCount = availableInCountTable.Single(k => k.HotelRoomId == hotelRoomId);
                    }
                    else
                    {
                        hotelRoomCount = new HotelRoomCount();
                    }


                    var staticRoomCount = db.Rooms.Single(room => room.Id == roomId).TotalRoom;

                    model.Reservation.HotelRoomIdStatic = hotelRoom.Id;
                    if (hotelRoomCount.Id == 0)
                    {


                        hotelRoomCount.Date = day;

                        hotelRoomCount.HotelRoomId = model.Reservation.HotelRoomIdStatic;
                        hotelRoomCount.TotalVacancyLeft = staticRoomCount - totalRoom;
                        if (staticRoomCount >= totalRoom * (model.Reservation.FromDate - model.Reservation.ToDate).Days)
                        {
                            db.HotelRoomCount.Add(hotelRoomCount);
                        }


                    }
                    else
                    {
                        if (totalRoom * (model.Reservation.FromDate - model.Reservation.ToDate).Days <=
                            hotelRoomCount.TotalVacancyLeft)
                        {
                            hotelRoomCount.TotalVacancyLeft = hotelRoomCount.TotalVacancyLeft - totalRoom;
                        }

                    }
                    db.SaveChanges();
                }
                model.Reservation.UserId = User.Identity.GetUserId();
                db.Reservations.Add(model.Reservation);
                db.SaveChanges();
                //return RedirectToAction("Success");
                return RedirectToAction("Success", model.Reservation);

            }
            model.Hotel = db.Hotels.SingleOrDefault(h => h.Id == model.Hotel.Id);
            model.Room = db.Rooms.ToList();
            model.Country = db.Countries.ToList();
            model.State = db.States.ToList();
            model.City = db.Cities.ToList();
            model.CreditCardType = new List<String> {"Master", "Visa", "American Express"};
            return View("View", model);
        }

        public ActionResult Success(Reservation reservation)
        {
            reservation.Customer = db.Customers.Single(r => r.Id == reservation.CustomerId);
           
            return View(reservation);
        }

        public ActionResult UserReservation()
        {
            var userId = User.Identity.GetUserId();
            var reservations =
                db.Reservations.Where(m => m.UserId == userId);
            if (reservations.Any())
            {
                List<ReservationDetailViewModel> reservationDetailViewModel = new List<ReservationDetailViewModel>();
                foreach (var reservation in reservations)
                {
                    var hotelRoom = db.HotelRooms.SingleOrDefault(hm => hm.Id == reservation.HotelRoomIdStatic);
                    reservationDetailViewModel.Add(new ReservationDetailViewModel
                    {
                        Hotel = db.Hotels.Single(h => h.Id == hotelRoom.HotelId),
                        Room = db.Rooms.Single(r => r.Id == hotelRoom.RoomId),
                        Customer = db.Customers.Single(c => c.Id == reservation.CustomerId),
                        Reservation = reservation,
                        Country = db.Countries.Single(c => c.Id == reservation.Customer.CountryId),
                        State = db.States.Single(c => c.Id == reservation.Customer.StateId),
                        City = db.Cities.Single(c => c.Id == reservation.Customer.CityId)
                    });

                }
                return View(reservationDetailViewModel);
            }
            else
            {
                return View();
            }
        }

        public ActionResult DeleteReservation(int id)
        {
            var reservation = db.Reservations.Single(r => r.Id == id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            var startDateOfreservation = reservation.FromDate;
            var endDateOfreservation = reservation.ToDate;
            var numberOfRooms = (Int32.Parse(reservation.NumberOFGuest)  % 2)+1;
            for (var day = startDateOfreservation; day.Date < endDateOfreservation; day = day.AddDays(1))
            {
                
                var hotelRoomCount = db.HotelRoomCount.Where(m => m.Date == day).Single(c => c.HotelRoomId == reservation.HotelRoomIdStatic);
                db.HotelRoomCount.Where(m => m.Date == day).Single(c => c.HotelRoomId == reservation.HotelRoomIdStatic)
                        .TotalVacancyLeft =
                    hotelRoomCount.TotalVacancyLeft  + numberOfRooms;
                db.SaveChanges();
            }
            db.Customers.Remove(db.Customers.Single(c => c.Id == reservation.CustomerId));
            
            db.Reservations.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("UserReservation");
        }


    }

}