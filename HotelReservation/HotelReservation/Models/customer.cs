﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
	public class Customer
	{

	    public int Id { get; set; }
        
        [DisplayName("Full Name")]
        [Required]
        public string Name { get; set; }
	    
	    [DisplayName("Street Number")]
        [Required(ErrorMessage = "Street Number Required")]
        public string StreetNumber { get; set; }
	    
        public City City { get; set; }
        [DisplayName("City")]
        public int CityId { get; set; }
	    
        public State State { get; set; }
        [DisplayName("State")]
        public int StateId { get; set; }

	    public Country Country { get; set; }

        [DisplayName("Country")]
        [Required]
        public int CountryId { get; set; }
	    
	    [DisplayName("Postal Code")]
        [Required]
        [ValidatePostal]
        public string PostalCode { get; set; }
	    
	    [DisplayName("Phone Number")]
        [MinLength(4)]
        [MaxLength(14)]
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number is not valid")]
        public string Phone { get; set; }
    }
}