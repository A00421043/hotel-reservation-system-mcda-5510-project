﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HotelReservation.Models
{
	public class Calendar
	{
		[Display(Name = "Expiry Date")]
		[DataType(DataType.Date)]
		public DateTime? ExDate { get; set; }
	}
}