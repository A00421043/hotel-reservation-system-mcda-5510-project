﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
    public class State
    {
        [DisplayName("State")]
        public int Id { get; set; }
        public string StateName { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
    }
}