﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
	public class Hotel
	{
	    public int Id { get; set; }
	    public String Name { get; set; }
	    public String Address { get; set; }
	    public Country Country { get; set; }
	    public int CountryId { get; set; }
	    public State State { get; set; }
	    public int StateId { get; set; }
        public City City { get; set; }
	    public int CityId { get; set; }
        public String Postal { get; set; }
	    public String Zip { get; set; }
	    public String PhoneNumber { get; set; }
	    public String WebUrl { get; set; }
	    public String PaymentMathod { get; set; }
	    public float Rating { get; set; }
	    public List<HotelRoom> HotelRoom { get; set; }

	    public int HotelRoomId { get; set; }
    }
}