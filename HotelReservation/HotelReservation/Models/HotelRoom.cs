﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
    public class HotelRoom
    {
        public int Id { get; set; }
        public Room Room { get; set; }
        public int RoomId { get; set; }
        public Hotel Hotel { get; set; }
        public int HotelId { get; set; }
    }
}