﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
	public class CreditCard
	{
        public int Id { get; set; }
        [Required]
        public string CreditType { get; set; }
        //[CreditCard(AcceptedCardTypes = CreditCardAttribute.CreditType.Visa | CreditCardAttribute.CreditType.MasterCard)]
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[^;:!@#$%\^*+?\\/<>1234567890]*$", ErrorMessage = @"Credit card holder's name should not contain the following characters: ;:!@#$%^*+?\/<>1234567890")]
        [Display(Name = "Name on the Card")]
        public string CreditCardName { get; set; }
        [Required]
        [MinLength(16,ErrorMessage ="Credit Card Number Must be 16 Digits")]
        [MaxLength(16,ErrorMessage = "Credit Card Number Must be 16 Digits")]
		public string CreditCardNumber { get; set;}
		public DateTime ExpiryDate { get; set; }
	}
}