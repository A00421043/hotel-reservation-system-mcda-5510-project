﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
	public class CreditType
	{
		public string Master { get; set; }
		public string Visa { get; set; }
		public string AmericanExpress { get; set; }

	}
}