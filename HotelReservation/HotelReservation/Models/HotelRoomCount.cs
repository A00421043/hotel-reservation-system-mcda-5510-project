﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
    public class HotelRoomCount
    {
        public int Id { get; set; }
        public int HotelRoomId { get; set; }
        public DateTime Date { get; set; }
        public int TotalVacancyLeft { get; set; }
    }
}