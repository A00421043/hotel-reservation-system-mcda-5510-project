﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Foolproof;
using HotelReservation.Views.Reservations;

namespace HotelReservation.Models
{
    public class Reservation
    {
        public int Id { get; set; }

        //public HotelRoom HotelRoom { get; set; }
        [Required(ErrorMessage = "Please Select A Room Type")]
        public int HotelRoomIdStatic { get; set; }
        //public Room Room { get; set; }
        //public int RoomId { get; set; }
        //public Hotel Hotel { get; set; }
        //public int HotelId { get; set; }
        [DataType(DataType.Date)]
       // [ValidateFromToDate]
        [Required(ErrorMessage = "Please enter checkin date")]
        public DateTime FromDate { get; set; }

        [GreaterThan("FromDate",ErrorMessage ="Check in date must be before check out date.")]
        [Required(ErrorMessage = "Please enter checkout date")]
        [DataType(DataType.Date)]
        //[ValidateFromToDate]
        public DateTime ToDate { get; set; }
        [Required(ErrorMessage ="Please enter number of Guests")]
        [Range(1, 10)]
        public string NumberOFGuest { get; set; }
        
        public string NumberOFRooms { get; set; }
        
        public Customer Customer { get; set; }
      
        public int CustomerId { get; set; }
        public string CreditCardType { get; set; }
        public CreditCard CreditCard { get; set; }
        public int CreditCardId { get; set; }
       
        public string UserId { get; set; }
    }
}   