﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservation.Models
{
    public class Room
    {
        

        public int Id { get; set; }
       
        public string RoomType { get; set; }
        public double Price { get; set; }

        public int TotalRoom { get; set; }
    }
}