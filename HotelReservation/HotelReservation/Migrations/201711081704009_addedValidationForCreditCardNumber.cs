namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedValidationForCreditCardNumber : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCards", "CreditCardName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "CreditCardName", c => c.String(nullable: false));
        }
    }
}
