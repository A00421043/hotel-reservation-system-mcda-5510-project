namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStateAndCity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "City_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "State_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "City_Id");
            CreateIndex("dbo.AspNetUsers", "State_Id");
            AddForeignKey("dbo.AspNetUsers", "City_Id", "dbo.Cities", "Id");
            AddForeignKey("dbo.AspNetUsers", "State_Id", "dbo.States", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "State_Id", "dbo.States");
            DropForeignKey("dbo.AspNetUsers", "City_Id", "dbo.Cities");
            DropIndex("dbo.AspNetUsers", new[] { "State_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "City_Id" });
            DropColumn("dbo.AspNetUsers", "State_Id");
            DropColumn("dbo.AspNetUsers", "City_Id");
        }
    }
}
