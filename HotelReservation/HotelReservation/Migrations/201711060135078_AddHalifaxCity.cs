namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHalifaxCity : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[Cities] ( [Name], [StateId]) VALUES ( 'Halifax', 669)");
        }
        
        public override void Down()
        {
        }
    }
}
