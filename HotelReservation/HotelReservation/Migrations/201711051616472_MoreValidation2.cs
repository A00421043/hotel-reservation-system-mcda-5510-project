namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreValidation2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "Phone", c => c.String(nullable: false, maxLength: 14));
            AlterColumn("dbo.CreditCards", "CreditType", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "CreditType", c => c.String());
            AlterColumn("dbo.Customers", "Phone", c => c.String(maxLength: 14));
        }
    }
}
