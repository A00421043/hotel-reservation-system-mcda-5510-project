namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedCustomer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Customers", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Customers", "State_Id", "dbo.States");
            DropIndex("dbo.Customers", new[] { "Country_Id" });
            DropIndex("dbo.Customers", new[] { "State_Id" });
            RenameColumn(table: "dbo.Customers", name: "Country_Id", newName: "CountryId");
            RenameColumn(table: "dbo.Customers", name: "State_Id", newName: "StateId");
            AlterColumn("dbo.Customers", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "StreetNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "PostalCode", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "CountryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Customers", "StateId", c => c.Int(nullable: false));
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String(nullable: false));
            AlterColumn("dbo.Reservations", "NumberOFRooms", c => c.String(nullable: false));
            CreateIndex("dbo.Customers", "StateId");
            CreateIndex("dbo.Customers", "CountryId");
            AddForeignKey("dbo.Customers", "CountryId", "dbo.Countries", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Customers", "StateId", "dbo.States", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "StateId", "dbo.States");
            DropForeignKey("dbo.Customers", "CountryId", "dbo.Countries");
            DropIndex("dbo.Customers", new[] { "CountryId" });
            DropIndex("dbo.Customers", new[] { "StateId" });
            AlterColumn("dbo.Reservations", "NumberOFRooms", c => c.String());
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String());
            AlterColumn("dbo.Customers", "StateId", c => c.Int());
            AlterColumn("dbo.Customers", "CountryId", c => c.Int());
            AlterColumn("dbo.Customers", "Phone", c => c.String());
            AlterColumn("dbo.Customers", "PostalCode", c => c.String());
            AlterColumn("dbo.Customers", "StreetNumber", c => c.String());
            AlterColumn("dbo.Customers", "Name", c => c.String());
            RenameColumn(table: "dbo.Customers", name: "StateId", newName: "State_Id");
            RenameColumn(table: "dbo.Customers", name: "CountryId", newName: "Country_Id");
            CreateIndex("dbo.Customers", "State_Id");
            CreateIndex("dbo.Customers", "Country_Id");
            AddForeignKey("dbo.Customers", "State_Id", "dbo.States", "Id");
            AddForeignKey("dbo.Customers", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
