namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels");
            DropForeignKey("dbo.AspNetUsers", "City_Id", "dbo.Cities");
            DropForeignKey("dbo.AspNetUsers", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.AspNetUsers", "State_Id", "dbo.States");
            DropIndex("dbo.Reservations", new[] { "HotelId" });
            DropIndex("dbo.AspNetUsers", new[] { "CountryId" });
            DropIndex("dbo.AspNetUsers", new[] { "City_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "State_Id" });
            AddColumn("dbo.Reservations", "HotelRoomId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "HotelRoomId");
            AddForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms", "Id", cascadeDelete: true);
            DropColumn("dbo.Reservations", "HotelId");
            DropColumn("dbo.AspNetUsers", "City_Id");
            DropColumn("dbo.AspNetUsers", "State_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "State_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "City_Id", c => c.Int());
            AddColumn("dbo.Reservations", "HotelId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms");
            DropIndex("dbo.Reservations", new[] { "HotelRoomId" });
            DropColumn("dbo.Reservations", "HotelRoomId");
            CreateIndex("dbo.AspNetUsers", "State_Id");
            CreateIndex("dbo.AspNetUsers", "City_Id");
            CreateIndex("dbo.AspNetUsers", "CountryId");
            CreateIndex("dbo.Reservations", "HotelId");
            AddForeignKey("dbo.AspNetUsers", "State_Id", "dbo.States", "Id");
            AddForeignKey("dbo.AspNetUsers", "CountryId", "dbo.Countries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUsers", "City_Id", "dbo.Cities", "Id");
            AddForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
    }
}
