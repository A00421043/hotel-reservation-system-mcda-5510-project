namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1029 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "GuestId", "dbo.Guests");
            DropForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms");
            DropIndex("dbo.Reservations", new[] { "HotelRoomId" });
            DropIndex("dbo.Reservations", new[] { "GuestId" });
            AddColumn("dbo.Reservations", "RoomType", c => c.String());
            AddColumn("dbo.Reservations", "HotelName", c => c.String());
            AddColumn("dbo.Reservations", "GuestAmount", c => c.String());
            AddColumn("dbo.Reservations", "RoomAmount", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_CreditType", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_CreditCardName", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_CreditCardNumber", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_ExpiryDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Reservations", "Customer_Id", c => c.Int());
            CreateIndex("dbo.Reservations", "Customer_Id");
            AddForeignKey("dbo.Reservations", "Customer_Id", "dbo.Customers", "Id");
            DropColumn("dbo.Reservations", "HotelRoomId");
            DropColumn("dbo.Reservations", "GuestId");
            DropTable("dbo.Guests");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Guests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PublicIdNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Reservations", "GuestId", c => c.Int(nullable: false));
            AddColumn("dbo.Reservations", "HotelRoomId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Reservations", "Customer_Id", "dbo.Customers");
            DropIndex("dbo.Reservations", new[] { "Customer_Id" });
            DropColumn("dbo.Reservations", "Customer_Id");
            DropColumn("dbo.Reservations", "CreditCard_ExpiryDate");
            DropColumn("dbo.Reservations", "CreditCard_CreditCardNumber");
            DropColumn("dbo.Reservations", "CreditCard_CreditCardName");
            DropColumn("dbo.Reservations", "CreditCard_CreditType");
            DropColumn("dbo.Reservations", "RoomAmount");
            DropColumn("dbo.Reservations", "GuestAmount");
            DropColumn("dbo.Reservations", "HotelName");
            DropColumn("dbo.Reservations", "RoomType");
            CreateIndex("dbo.Reservations", "GuestId");
            CreateIndex("dbo.Reservations", "HotelRoomId");
            AddForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reservations", "GuestId", "dbo.Guests", "Id", cascadeDelete: true);
        }
    }
}
