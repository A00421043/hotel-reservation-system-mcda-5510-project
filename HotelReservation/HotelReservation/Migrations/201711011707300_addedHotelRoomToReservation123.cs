namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedHotelRoomToReservation123 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms");
            DropIndex("dbo.Reservations", new[] { "HotelRoomId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Reservations", "HotelRoomId");
            AddForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms", "Id", cascadeDelete: true);
        }
    }
}
