namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedHotelModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotels", "CountryId", c => c.Int(nullable: false));
            AddColumn("dbo.Hotels", "StateId", c => c.Int(nullable: false));
            AddColumn("dbo.Hotels", "CityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Hotels", "CountryId");
            CreateIndex("dbo.Hotels", "StateId");
            CreateIndex("dbo.Hotels", "CityId");
            AddForeignKey("dbo.Hotels", "CityId", "dbo.Cities", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Hotels", "CountryId", "dbo.Countries", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Hotels", "StateId", "dbo.States", "Id", cascadeDelete: false);
            DropColumn("dbo.Hotels", "City");
            DropColumn("dbo.Hotels", "Country");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Hotels", "Country", c => c.String());
            AddColumn("dbo.Hotels", "City", c => c.String());
            DropForeignKey("dbo.Hotels", "StateId", "dbo.States");
            DropForeignKey("dbo.Hotels", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Hotels", "CityId", "dbo.Cities");
            DropIndex("dbo.Hotels", new[] { "CityId" });
            DropIndex("dbo.Hotels", new[] { "StateId" });
            DropIndex("dbo.Hotels", new[] { "CountryId" });
            DropColumn("dbo.Hotels", "CityId");
            DropColumn("dbo.Hotels", "StateId");
            DropColumn("dbo.Hotels", "CountryId");
        }
    }
}
