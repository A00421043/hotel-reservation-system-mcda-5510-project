namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedHotelRooms : DbMigration
    {
        public override void Up()
        {
            Sql(@"insert into hotelRooms Values(4,1);
            insert into hotelRooms Values(4,2);
            insert into hotelRooms Values(4,3);
            insert into hotelRooms Values(4,4);
            insert into hotelRooms Values(5,1);
            insert into hotelRooms Values(5,2);
            insert into hotelRooms Values(5,3);
            insert into hotelRooms Values(5,4);
            insert into hotelRooms Values(6,1);
            insert into hotelRooms Values(6,2);
            insert into hotelRooms Values(6,3);
            insert into hotelRooms Values(6,4);
            insert into hotelRooms Values(7,1);
            insert into hotelRooms Values(7,2);
            insert into hotelRooms Values(7,3);
            insert into hotelRooms Values(7,4);
            insert into hotelRooms Values(8,1);
            insert into hotelRooms Values(8,2);
            insert into hotelRooms Values(8,3);
            insert into hotelRooms Values(8,4);
            insert into hotelRooms Values(9,1);
            insert into hotelRooms Values(9,2);
            insert into hotelRooms Values(9,3);
            insert into hotelRooms Values(9,4);
            insert into hotelRooms Values(10,1);
            insert into hotelRooms Values(10,2);
            insert into hotelRooms Values(10,3);
            insert into hotelRooms Values(10,4);
            insert into hotelRooms Values(11,1);
            insert into hotelRooms Values(11,2);
            insert into hotelRooms Values(11,3);
            insert into hotelRooms Values(11,4);
            insert into hotelRooms Values(12,1);
            insert into hotelRooms Values(12,2);
            insert into hotelRooms Values(12,3);
            insert into hotelRooms Values(12,4);
            insert into hotelRooms Values(13,1);
            insert into hotelRooms Values(13,2);
            insert into hotelRooms Values(13,3);
            insert into hotelRooms Values(13,4);
            insert into hotelRooms Values(14,1);
            insert into hotelRooms Values(14,2);
            insert into hotelRooms Values(14,3);
            insert into hotelRooms Values(14,4);
            insert into hotelRooms Values(15,1);
            insert into hotelRooms Values(15,2);
            insert into hotelRooms Values(15,3);
            insert into hotelRooms Values(15,4);
            insert into hotelRooms Values(16,1);
            insert into hotelRooms Values(16,2);
            insert into hotelRooms Values(16,3);
            insert into hotelRooms Values(16,4);
            insert into hotelRooms Values(17,1);
            insert into hotelRooms Values(17,2);
            insert into hotelRooms Values(17,3);
            insert into hotelRooms Values(17,4);
            insert into hotelRooms Values(18,1);
            insert into hotelRooms Values(18,2);
            insert into hotelRooms Values(18,3);
            insert into hotelRooms Values(18,4);
            insert into hotelRooms Values(19,1);
            insert into hotelRooms Values(19,2);
            insert into hotelRooms Values(19,3);
            insert into hotelRooms Values(19,4);
            insert into hotelRooms Values(20,1);
            insert into hotelRooms Values(20,2);
            insert into hotelRooms Values(20,3);
            insert into hotelRooms Values(20,4);
            "
            );
        }
        
        public override void Down()
        {
        }
    }
}
