namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateReservationModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels");
            DropForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms");
            DropIndex("dbo.Reservations", new[] { "RoomId" });
            DropIndex("dbo.Reservations", new[] { "HotelId" });
            AddColumn("dbo.Reservations", "HotelRoomId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "HotelRoomId");
            AddForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms", "Id", cascadeDelete: true);
            DropColumn("dbo.Reservations", "RoomId");
            DropColumn("dbo.Reservations", "HotelId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "HotelId", c => c.Int(nullable: false));
            AddColumn("dbo.Reservations", "RoomId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms");
            DropIndex("dbo.Reservations", new[] { "HotelRoomId" });
            DropColumn("dbo.Reservations", "HotelRoomId");
            CreateIndex("dbo.Reservations", "HotelId");
            CreateIndex("dbo.Reservations", "RoomId");
            AddForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
    }
}
