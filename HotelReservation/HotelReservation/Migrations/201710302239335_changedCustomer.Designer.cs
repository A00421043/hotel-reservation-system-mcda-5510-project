// <auto-generated />
namespace HotelReservation.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class changedCustomer : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(changedCustomer));
        
        string IMigrationMetadata.Id
        {
            get { return "201710302239335_changedCustomer"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
