namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populateUser : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Name], [DateOfBirth], [Gender], [CountryId]) VALUES (N'54aa4bea-6e91-47fb-a073-577af77a689c', N'nitin.akash1989@gmail.com', 0, N'AAy81PVSQxytAo1WCdU/8XgIpm9ZTbmy8vsEhXZyrZzf2KhmoYpWwbl3zG3VF4UAag==', N'ed5aefc5-03d5-4d23-914a-c7c008fac978', N'27011989', 0, 0, NULL, 1, 0, N'nitin.akash1989@gmail.com', N'Nitin Akash', N'1989-01-27 00:00:00', N'Male', 101)
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Name], [DateOfBirth], [Gender], [CountryId]) VALUES (N'a1816848-2c21-4081-93ca-ea33a0343f4d', N'nitin.akash2017@gmail.com', 0, N'AD6OytrzpJu5Bjaps7lje0QQ9RbpU3UsN/xNkIWlmh0Wh9KM7fmTsNQza0c4yjvtkw==', N'8c598f9c-2af2-46c8-bfff-f1d97f1985c9', N'9024998927', 0, 0, NULL, 1, 0, N'nitin.akash2017@gmail.com', N'Nitin Akash', N'1989-01-27 00:00:00', N'Male', 101)
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'8103154d-1975-4778-848e-a3da18cdf986', N'ManageHotels')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'54aa4bea-6e91-47fb-a073-577af77a689c', N'8103154d-1975-4778-848e-a3da18cdf986')

");
        }
        
        public override void Down()
        {
        }
    }
}
