namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedUserIdentityToReservation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reservations", "UserId");
        }
    }
}
