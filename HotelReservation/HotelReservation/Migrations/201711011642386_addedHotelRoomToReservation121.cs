namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedHotelRoomToReservation121 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "HotelRoomId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "HotelRoomId");
            AddForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "HotelRoomId", "dbo.HotelRooms");
            DropIndex("dbo.Reservations", new[] { "HotelRoomId" });
            DropColumn("dbo.Reservations", "HotelRoomId");
        }
    }
}
