namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingReservationRoomCascadeDelete : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "RoomId", c => c.Int(nullable: false));
            //AddForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms", "Id", cascadeDelete: true);
            
        }

        public override void Down()
        {
            //DropForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms");
            //DropIndex("dbo.Reservations", new[] { "RoomId" });
            //DropColumn("dbo.Reservations", "RoomId");
        }
    }
}
