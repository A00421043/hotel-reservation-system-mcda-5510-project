namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreditType = c.String(),
                        CreditCardName = c.String(),
                        CreditCardNumber = c.String(),
                        ExpiryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Reservations", "CreditCard_Id", c => c.Int());
            CreateIndex("dbo.Reservations", "CreditCard_Id");
            AddForeignKey("dbo.Reservations", "CreditCard_Id", "dbo.CreditCards", "Id");
            DropColumn("dbo.Reservations", "CreditCard_CreditType");
            DropColumn("dbo.Reservations", "CreditCard_CreditCardName");
            DropColumn("dbo.Reservations", "CreditCard_CreditCardNumber");
            DropColumn("dbo.Reservations", "CreditCard_ExpiryDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "CreditCard_ExpiryDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Reservations", "CreditCard_CreditCardNumber", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_CreditCardName", c => c.String());
            AddColumn("dbo.Reservations", "CreditCard_CreditType", c => c.String());
            DropForeignKey("dbo.Reservations", "CreditCard_Id", "dbo.CreditCards");
            DropIndex("dbo.Reservations", new[] { "CreditCard_Id" });
            DropColumn("dbo.Reservations", "CreditCard_Id");
            DropTable("dbo.CreditCards");
        }
    }
}
