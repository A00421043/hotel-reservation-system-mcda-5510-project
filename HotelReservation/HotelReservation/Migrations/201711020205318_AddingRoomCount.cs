namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingRoomCount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HotelRoomCounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HotelRoomId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        TotalVacancyLeft = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HotelRoomCounts");
        }
    }
}
