namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedReservation1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String());
            AlterColumn("dbo.Reservations", "NumberOFRooms", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reservations", "NumberOFRooms", c => c.String(nullable: false));
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String(nullable: false));
        }
    }
}
