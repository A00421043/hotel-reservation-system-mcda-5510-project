namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedRoomTypeToMandet : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Rooms", "RoomType", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rooms", "RoomType", c => c.String());
        }
    }
}
