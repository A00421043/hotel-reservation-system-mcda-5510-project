namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReservationAndGuestUpdated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Guests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PublicIdNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Reservations", "FromDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Reservations", "ToDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Reservations", "HotelId", c => c.Int(nullable: false));
            AddColumn("dbo.Reservations", "GuestId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "HotelId");
            CreateIndex("dbo.Reservations", "GuestId");
            AddForeignKey("dbo.Reservations", "GuestId", "dbo.Guests", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels");
            DropForeignKey("dbo.Reservations", "GuestId", "dbo.Guests");
            DropIndex("dbo.Reservations", new[] { "GuestId" });
            DropIndex("dbo.Reservations", new[] { "HotelId" });
            DropColumn("dbo.Reservations", "GuestId");
            DropColumn("dbo.Reservations", "HotelId");
            DropColumn("dbo.Reservations", "ToDate");
            DropColumn("dbo.Reservations", "FromDate");
            DropTable("dbo.Guests");
        }
    }
}
