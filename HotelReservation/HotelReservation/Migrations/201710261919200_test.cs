namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Hotels", "HotelRoomId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Hotels", "HotelRoomId", c => c.Int(nullable: false));
        }
    }
}
