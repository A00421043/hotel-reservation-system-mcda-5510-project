namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAnnottaionTOReservation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reservations", "CreditCardType", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reservations", "CreditCardType", c => c.String());
        }
    }
}
