namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "HotelId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "HotelId");
            AddForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "HotelId", "dbo.Hotels");
            DropIndex("dbo.Reservations", new[] { "HotelId" });
            DropColumn("dbo.Reservations", "HotelId");
        }
    }
}
