namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedHotelRoomToReservation1234 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "HotelRoomIdStatic", c => c.Int(nullable: false));
            DropColumn("dbo.Reservations", "HotelRoomId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "HotelRoomId", c => c.Int(nullable: false));
            DropColumn("dbo.Reservations", "HotelRoomIdStatic");
        }
    }
}
