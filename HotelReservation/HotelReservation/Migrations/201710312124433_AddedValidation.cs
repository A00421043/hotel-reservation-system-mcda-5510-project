namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "StreetNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reservations", "NumberOFGuest", c => c.String());
            AlterColumn("dbo.Customers", "StreetNumber", c => c.String());
            AlterColumn("dbo.Customers", "Name", c => c.String());
        }
    }
}
