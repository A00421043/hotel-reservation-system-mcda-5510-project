// <auto-generated />
namespace HotelReservation.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ReservationAndGuestUpdated : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ReservationAndGuestUpdated));
        
        string IMigrationMetadata.Id
        {
            get { return "201710231419345_ReservationAndGuestUpdated"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
