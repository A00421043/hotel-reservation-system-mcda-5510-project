namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyingUserModel : DbMigration
    {
        public override void Up()
        {
            //DropColumn("dbo.AspNetUsers", "Name");
            //DropColumn("dbo.AspNetUsers", "DateOfBirth");
            //DropColumn("dbo.AspNetUsers", "Gender");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Gender", c => c.String());
            AddColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
        }
    }
}
