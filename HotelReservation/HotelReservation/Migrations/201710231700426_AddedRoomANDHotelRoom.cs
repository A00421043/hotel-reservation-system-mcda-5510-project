namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRoomANDHotelRoom : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HotelRooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Hotel_Id = c.Int(),
                        Room_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Hotels", t => t.Hotel_Id)
                .ForeignKey("dbo.Rooms", t => t.Room_Id)
                .Index(t => t.Hotel_Id)
                .Index(t => t.Room_Id);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoomType = c.String(),
                        Price = c.Double(nullable: false),
                        TotalRoom = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Hotels", "HotelRoomId", c => c.Int(nullable: false));
            DropColumn("dbo.Hotels", "RoomType");
            DropColumn("dbo.Hotels", "Vacancy");
            DropColumn("dbo.Hotels", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Hotels", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.Hotels", "Vacancy", c => c.Int(nullable: false));
            AddColumn("dbo.Hotels", "RoomType", c => c.String());
            DropForeignKey("dbo.HotelRooms", "Room_Id", "dbo.Rooms");
            DropForeignKey("dbo.HotelRooms", "Hotel_Id", "dbo.Hotels");
            DropIndex("dbo.HotelRooms", new[] { "Room_Id" });
            DropIndex("dbo.HotelRooms", new[] { "Hotel_Id" });
            DropColumn("dbo.Hotels", "HotelRoomId");
            DropTable("dbo.Rooms");
            DropTable("dbo.HotelRooms");
        }
    }
}
