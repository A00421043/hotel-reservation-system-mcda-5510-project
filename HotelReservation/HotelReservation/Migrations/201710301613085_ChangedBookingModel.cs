namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedBookingModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "CreditCard_Id", "dbo.CreditCards");
            DropForeignKey("dbo.Reservations", "Customer_Id", "dbo.Customers");
            DropIndex("dbo.Reservations", new[] { "CreditCard_Id" });
            DropIndex("dbo.Reservations", new[] { "Customer_Id" });
            RenameColumn(table: "dbo.Reservations", name: "CreditCard_Id", newName: "CreditCardId");
            RenameColumn(table: "dbo.Reservations", name: "Customer_Id", newName: "CustomerId");
            AddColumn("dbo.Reservations", "RoomId", c => c.Int(nullable: false));
            AddColumn("dbo.Reservations", "NumberOFGuest", c => c.String());
            AddColumn("dbo.Reservations", "NumberOFRooms", c => c.String());
            AddColumn("dbo.Reservations", "CreditCardType", c => c.String());
            AlterColumn("dbo.Reservations", "CreditCardId", c => c.Int(nullable: false));
            AlterColumn("dbo.Reservations", "CustomerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "RoomId");
            CreateIndex("dbo.Reservations", "CustomerId");
            CreateIndex("dbo.Reservations", "CreditCardId");
            AddForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reservations", "CreditCardId", "dbo.CreditCards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reservations", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
            DropColumn("dbo.Reservations", "RoomType");
            DropColumn("dbo.Reservations", "HotelName");
            DropColumn("dbo.Reservations", "GuestAmount");
            DropColumn("dbo.Reservations", "RoomAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "RoomAmount", c => c.String());
            AddColumn("dbo.Reservations", "GuestAmount", c => c.String());
            AddColumn("dbo.Reservations", "HotelName", c => c.String());
            AddColumn("dbo.Reservations", "RoomType", c => c.String());
            DropForeignKey("dbo.Reservations", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Reservations", "CreditCardId", "dbo.CreditCards");
            DropForeignKey("dbo.Reservations", "RoomId", "dbo.Rooms");
            DropIndex("dbo.Reservations", new[] { "CreditCardId" });
            DropIndex("dbo.Reservations", new[] { "CustomerId" });
            DropIndex("dbo.Reservations", new[] { "RoomId" });
            AlterColumn("dbo.Reservations", "CustomerId", c => c.Int());
            AlterColumn("dbo.Reservations", "CreditCardId", c => c.Int());
            DropColumn("dbo.Reservations", "CreditCardType");
            DropColumn("dbo.Reservations", "NumberOFRooms");
            DropColumn("dbo.Reservations", "NumberOFGuest");
            DropColumn("dbo.Reservations", "RoomId");
            RenameColumn(table: "dbo.Reservations", name: "CustomerId", newName: "Customer_Id");
            RenameColumn(table: "dbo.Reservations", name: "CreditCardId", newName: "CreditCard_Id");
            CreateIndex("dbo.Reservations", "Customer_Id");
            CreateIndex("dbo.Reservations", "CreditCard_Id");
            AddForeignKey("dbo.Reservations", "Customer_Id", "dbo.Customers", "Id");
            AddForeignKey("dbo.Reservations", "CreditCard_Id", "dbo.CreditCards", "Id");
        }
    }
}
