namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedHotels : DbMigration
    {
        public override void Up()
        {
            Sql(@"Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Sheraton','Geneva',38,669,48316,'B3H 1T3','702 801 301',4);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Royal','Harvester ',38,671,10443,'L5A 9H9','902 542 6584',2);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Redmont','PHOENIX AZ 85123',231,3956,48019,'85123',' 734 535 1312',1);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Fairmont','Bonnyville',38,675,10868,'B3H 5T3','604 245 2635',3);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Ritz Carlton','Bromont',38,669,48316,'B9H 1T3','604 264 5612',5);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Colony ','SEATTLE WA 98102',231,3934,44173,'98102','743  655 1712',1);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Rosewood','Binome',38,671,10443,'B3L 1T3','604 566 9599',2);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('DisneyLand','TUCSON AZ 85705',231,3930,43677,'85705','743 632 6413',3);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('WoodLand','SEATTLE WK 98652',231,3970,46516,'98652','743 256 5658',4);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Sofitel','Rossly',38,671,10520,'B8L 9T3','604 598 9826',3);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Andaz','TUCSON AZ 85705',231,3956,48019,'85705','743 521 3565',4);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Victoria','Labrosse ',38,673,10794,'L3O 1T3','604 485 9154',5);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Hugo','PHOENIX AZ 85123',231,3924,42865,'85123','743 551 548',5);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Hilton','TUCSON AZ 85709',231,3970,46516,'85709','743 659 846',4);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Sonora','Chain',38,669,48316,'L3H 1B3','604 598 5957',3);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Plaza','TUCSON AZ 75705',231,3930,43677,'75705','743 546 569',4);
Insert into Hotels(Name,Address,CountryId,StateId,CityId,Postal,PhoneNumber,Rating) Values('Novtel','Edgar',38,671,10520,'B0H 1T0','604 598 9952',4);
");
        }
        
        public override void Down()
        {
        }
    }
}
