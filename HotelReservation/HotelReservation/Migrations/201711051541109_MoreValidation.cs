namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCards", "CreditCardName", c => c.String(nullable: false));
            AlterColumn("dbo.CreditCards", "CreditCardNumber", c => c.String(nullable: false, maxLength: 16));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "CreditCardNumber", c => c.String());
            AlterColumn("dbo.CreditCards", "CreditCardName", c => c.String());
        }
    }
}
