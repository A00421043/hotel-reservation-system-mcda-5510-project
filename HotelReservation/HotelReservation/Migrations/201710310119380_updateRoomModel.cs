namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateRoomModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Rooms", "RoomType", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rooms", "RoomType", c => c.String(nullable: false));
        }
    }
}
