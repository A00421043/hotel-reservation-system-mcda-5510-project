namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateHotelRoomModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HotelRooms", "Hotel_Id", "dbo.Hotels");
            DropForeignKey("dbo.HotelRooms", "Room_Id", "dbo.Rooms");
            DropIndex("dbo.HotelRooms", new[] { "Hotel_Id" });
            DropIndex("dbo.HotelRooms", new[] { "Room_Id" });
            RenameColumn(table: "dbo.HotelRooms", name: "Hotel_Id", newName: "HotelId");
            RenameColumn(table: "dbo.HotelRooms", name: "Room_Id", newName: "RoomId");
            AlterColumn("dbo.HotelRooms", "HotelId", c => c.Int(nullable: false));
            AlterColumn("dbo.HotelRooms", "RoomId", c => c.Int(nullable: false));
            CreateIndex("dbo.HotelRooms", "RoomId");
            CreateIndex("dbo.HotelRooms", "HotelId");
            AddForeignKey("dbo.HotelRooms", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HotelRooms", "RoomId", "dbo.Rooms", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelRooms", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.HotelRooms", "HotelId", "dbo.Hotels");
            DropIndex("dbo.HotelRooms", new[] { "HotelId" });
            DropIndex("dbo.HotelRooms", new[] { "RoomId" });
            AlterColumn("dbo.HotelRooms", "RoomId", c => c.Int());
            AlterColumn("dbo.HotelRooms", "HotelId", c => c.Int());
            RenameColumn(table: "dbo.HotelRooms", name: "RoomId", newName: "Room_Id");
            RenameColumn(table: "dbo.HotelRooms", name: "HotelId", newName: "Hotel_Id");
            CreateIndex("dbo.HotelRooms", "Room_Id");
            CreateIndex("dbo.HotelRooms", "Hotel_Id");
            AddForeignKey("dbo.HotelRooms", "Room_Id", "dbo.Rooms", "Id");
            AddForeignKey("dbo.HotelRooms", "Hotel_Id", "dbo.Hotels", "Id");
        }
    }
}
