namespace HotelReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "CityId", c => c.Int(nullable: false));
            AddColumn("dbo.Customers", "Country_Id", c => c.Int());
            AddColumn("dbo.Customers", "State_Id", c => c.Int());
            CreateIndex("dbo.Customers", "CityId");
            CreateIndex("dbo.Customers", "Country_Id");
            CreateIndex("dbo.Customers", "State_Id");
            AddForeignKey("dbo.Customers", "CityId", "dbo.Cities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Customers", "Country_Id", "dbo.Countries", "Id");
            AddForeignKey("dbo.Customers", "State_Id", "dbo.States", "Id");
            DropColumn("dbo.Customers", "City");
            DropColumn("dbo.Customers", "Province");
            DropColumn("dbo.Customers", "Country");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Country", c => c.String());
            AddColumn("dbo.Customers", "Province", c => c.String());
            AddColumn("dbo.Customers", "City", c => c.String());
            DropForeignKey("dbo.Customers", "State_Id", "dbo.States");
            DropForeignKey("dbo.Customers", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Customers", "CityId", "dbo.Cities");
            DropIndex("dbo.Customers", new[] { "State_Id" });
            DropIndex("dbo.Customers", new[] { "Country_Id" });
            DropIndex("dbo.Customers", new[] { "CityId" });
            DropColumn("dbo.Customers", "State_Id");
            DropColumn("dbo.Customers", "Country_Id");
            DropColumn("dbo.Customers", "CityId");
        }
    }
}
